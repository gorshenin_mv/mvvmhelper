������������� � dll
��� ���������� ������������ ���� � xaml-�������� ������������� ������������ 
URL namespace xmlns:mvvm="http://schema/MG/MVVMHelper"

������������� ������:
===========================
Command <object>: ������������ ��� �������� ������

������ �������������:
xaml:
<Button ...
		Command="{Binding Click}"
		CommandParameter="10"/>
ViewModel:
private ICommand click;
public ICommand Click
{
	get 
	{
		if (click==null)
			click=new Command<object>(_Click)
		return click;
	}
}

public void _Click(object sender)
{
	Console.WriteLine((int)sender) // ����� ����� 1
	...
}
============================
NotifyPropertyChangedAddIn - ����������� �����, ����������� � ����������� ��������� INotifyPropertyChanged

������ �������������:
internal class Man : NotifyPropertyChangedAddIn
{
	private string name;
	public string Name
	{
		get {return name;}
		set {name=value;
		     OnPropertyChanged("Name");}
	}
} 
=============================
EventCommand - ��������� � �������� ��������� ������� �������� ������ �� ��������� ������, ����������� �� EventArgs, 
������� ���������� ������ � ��������

������ �������������:
xaml:
xmlns:mvvm="http://schema/MG/MVVMHelper"
xmlns:i="http://schemas.microsoft.com/expression/2010/interactivity"

<ComboBox ...>
            <i:Interaction.Triggers>
                <i:EventTrigger EventName="SelectionChanged">
                    <mvvm:EventCommand Command="{Binding TestTrigger}" 
                                       CommandParameter="{Binding RelativeSource={RelativeSource Self}, Path=InvokeParameter}"/>
                </i:EventTrigger>
            </i:Interaction.Triggers>
</ComboBox>

ViewModel:
private ICommand testTrigger;
public ICommand TestTrigger
{
	get 
	{
		if (testTrigger==null)
			testTrigger=new Command<object>(_TestTrigger)
		return testTrigger;
	}
}

public void _TestTrigger(object sender)
{
	// sender = SelectionChangedEventArgs e
	...
}